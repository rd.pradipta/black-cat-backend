// Required Libraries
const url = require('url');
const path = require('path')
const moment = require('moment')
const fileSystem = require('fs');
const jsonServer = require('json-server')

// Json Server
const server = jsonServer.create()
const databaseRelativePath = path.join(__dirname, 'db.json')
const router = jsonServer.router(databaseRelativePath)
const middlewares = jsonServer.defaults()
const rewriter = jsonServer.rewriter({})

server.use(middlewares)
server.use(jsonServer.bodyParser)

// ========== GENERAL FUNCTIONS ========== //

getJSONData = () => {
  return JSON.parse(fileSystem.readFileSync(databaseRelativePath).toString())
}

getIdFromURL = (req, slicingPoint) => {
  return Number(url.parse(req.url).path.split('/')[slicingPoint])
}

getEventAndUserId = (req) => {
  const parsed = url.parse(req.url).path.split('/')
  return [Number(parsed[2]), Number(parsed[4])]
}

getMiniUserDetailsObj = (objectToCompare) => {
  const JSONData = getJSONData()
  const userObj = JSONData["users"].find(obj => obj.id === objectToCompare.user_id);
  return {
    "id": userObj.id,
    "username": userObj.username,
    "email": userObj.email,
    "avatar": userObj.avatar
  }
}

getUserDetails = (objectToCompare) => {
  let usersDetail = []
  objectToCompare.forEach((data) => {
    usersDetail.push(getMiniUserDetailsObj(data))
  });
  return usersDetail
}

// To be Used by POST Likes or Self Participant
generateEventParticipationOrLikeObj = (req, objParam) => {
  const JSONData = getJSONData()
  const eventId = getIdFromURL(req, 2)
  JSONData[objParam].push(
    {
      event_id: eventId,
      user_id: req.body.user_id
    }
  )
  return JSONData
}

// To be Used by DELETE Likes or Self Participant
generateNewParticipantsOrLikesByUserId = (req, objParam) => {
  const JSONData = getJSONData()
  const [eventId, userId] = getEventAndUserId(req)
  let newGeneratedData = []
  for (let data of JSONData[objParam]) {
    if(!(data["event_id"] === eventId && data["user_id"] === userId)){
      newGeneratedData.push(data)
    }
  }
  JSONData[objParam] = newGeneratedData
  return JSONData
}

// Manipulation to get is user liked an event or joined an event
getIsUserParticipatedOrLikedAnEvent = (eventId, userId) => {
  const JSONData = getJSONData()
  
  // Get is current user a participant of this event or not
  let userParticipations = JSONData["participants"].filter(obj => {
    return obj.user_id === userId
  })
  let participationResult = userParticipations.filter(obj => {
    return obj.event_id === eventId
  })

  // Get is current user likes this event or not
  let userLikes = JSONData["likes"].filter(obj => {
    return obj.user_id === userId
  })
  let likeResult = userLikes.filter(obj => {
    return obj.event_id === eventId
  })

  return {
    is_current_user_participant: participationResult.length,
    is_current_user_like: likeResult.length
  }
}

// To be Used by GET User's Likes or Self Participant Events
generateEventAndChannelForParticipantAndLikes = (req, objParam) => {
  const JSONData = getJSONData()
  let allData = JSONData[objParam].filter(obj => {
    return obj.user_id === getIdFromURL(req, 2)
  })
  allData.forEach((data) => {
    const eventObj = JSONData["events"].find(obj => obj.id === data.event_id)
    const channelObj = JSONData["channels"].find(obj => obj.id === eventObj.channel_id);
    eventObj["channel_name"] = channelObj.name
    eventObj["creator"] = getMiniUserDetailsObj(eventObj)

    // Participant Counter of an event
    let participants = JSONData["participants"].filter(obj => {
      return obj.event_id === eventObj.id
    })
    eventObj["participants_count"] = getUserDetails(participants).length
    // Like Counter of an event
    let likes = JSONData["likes"].filter(obj => {
      return obj.event_id === eventObj.id
    })
    eventObj["likes_count"] = getUserDetails(likes).length

    userActivity = getIsUserParticipatedOrLikedAnEvent(eventObj.id, getIdFromURL(req, 2))
    Object.assign(eventObj, userActivity);

    // Remove unused key-value pair
    delete Object.assign(data, {["id"]: data["event_id"] })["event_id"];  // Change event_id to id to make it consistent    
    delete eventObj["id"];
    delete eventObj["user_id"];
    delete eventObj["create_time"];
    delete eventObj["location"];

    Object.assign(data, eventObj);
  })
  return allData
}

doWriteToJSON = (JSONData) => {
  fileSystem.writeFileSync(databaseRelativePath, JSON.stringify(JSONData, null, 2));
}

// ========== AUTH SPECIFIC APIs ========== //

/*
POST user credentials & get token API. Example Response:
{
  "user_id": 101,
  "token": "USER-101TOKEN"
  "user" {
    "id": 101,
    "username": "conan_edogawa",
    "email": "conan_edogawa@email.com"
    "avatar": "http://i.pravatar.cc/300?u=ConanEdogawa"
  }
}
*/
server.post('/auth/token', (req, res) => {
  const JSONData = getJSONData()
  if (req.body.username === req.body.password) {
    let tokenObj = {}
    const userIDObj = JSONData["users"].find(obj => obj.username === req.body.username);  // If user not found, this object will return undefined
    if (userIDObj){
      tokenObj = JSONData["tokens"].find(obj => obj.user_id === userIDObj["id"])
      tokenObj["user"] = userIDObj
      delete tokenObj["user"]["password"]
      res.json(tokenObj)
    }
  } else {
    res.sendStatus(404)
  }
});

// ========== EVENT SPECIFIC APIs ========== //

/*
GET All Events. Example Response:
[
  {
    "id": 1001,
    "user_id": 101,
    "channel_id": 1,
    "title": "Annual Gaming Exhibition 2022",
    "start_time": "2022-12-01T10:00:00Z",
    "end_time": "2022-12-11T10:00:00Z",
    "description": "EVENT 1001 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam pellentesque sollicitudin nunc id tristique. Morbi ut enim nec eros semper imperdiet eu eu mi. Sed aliquam non turpis non elementum. Praesent tempus, felis nec porttitor posuere, nisi ante pulvinar elit, vitae semper justo justo luctus urna. Proin dignissim nisi ut eros mollis ultricies. Sed tincidunt metus quis euismod gravida. Aenean ac dictum mi, sed consequat nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi feugiat, sem vel vehicula egestas, diam neque sollicitudin nulla, cursus tristique tellus magna id erat. Nam congue urna at posuere blandit. Duis a enim id arcu ullamcorper rhoncus. Aliquam sit amet nisl sed libero pellentesque volutpat id eu magna. In viverra malesuada neque quis commodo. Fusce quis rutrum turpis. Aliquam erat volutpat. Suspendisse gravida ipsum turpis, sed finibus quam tristique sit amet. Etiam accumsan dolor justo, ut tempor elit fringilla eget. Nunc iaculis felis est, vel tristique purus lobortis eget.",
    "channel_name": "Gaming",
    "creator": {
      "id": 101,
      "username": "conan_edogawa",
      "avatar": "http://i.pravatar.cc/300?u=ConanEdogawa"
    },
    "participants_count": 3,
    "likes_count": 1
    "is_current_user_participant": 1
    "is_current_user_like": 1
  }
]
*/
server.get('/events/:userId', (req, res) => {
  const JSONData = getJSONData()
  let eventObj = JSONData["events"]
  eventObj.forEach((event) => {
    const channelObj = JSONData["channels"].find(obj => obj.id === event.channel_id);
    let participants = JSONData["participants"].filter(obj => {
      return obj.event_id === event.id
    })
    event["channel_name"] = channelObj.name
    event["creator"] = getMiniUserDetailsObj(event)
    event["participants_count"] = getUserDetails(participants).length
    let likes = JSONData["likes"].filter(obj => {
      return obj.event_id === event.id
    })
    event["likes_count"] = getUserDetails(likes).length

    userActivity = getIsUserParticipatedOrLikedAnEvent(event.id, getIdFromURL(req, 2))
    Object.assign(event, userActivity);

    // Remove unused key-value pair
    delete event["create_time"];
    delete event["location"];    
  });
  res.json(eventObj)
});

/*
GET Specific Event Details (Page Header). Example Response:
{
  "channel_name": "Gaming",
  "title": "Annual Gaming Exhibition 2022",
  "creator": {
    "id": 101,
    "username": "conan_edogawa",
    "avatar": "http://i.pravatar.cc/300?u=ConanEdogawa"
  },
  "create_time": "2022-10-01T10:00:00Z"
}
*/
server.get('/event/:eventId/headers', (req, res) => {
  const JSONData = getJSONData()
  const eventObj = JSONData["events"].find(obj => obj.id === getIdFromURL(req, 2))
  const channelObj = JSONData["channels"].find(obj => obj.id === eventObj.channel_id)
  res.json({
    channel_name: channelObj.name,
    title: eventObj.title,
    creator: getMiniUserDetailsObj(eventObj),
    create_time: eventObj.create_time
  })
})

/*
GET Specific Event Details (Details Subpage). Example Response:
{
  "start_time": "2022-12-01T10:00:00Z",
  "end_time": "2022-12-11T10:00:00Z",
  "location": "Jakarta",
  "description": "EVENT 1001 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam pellentesque sollicitudin nunc id tristique. Morbi ut enim nec eros semper imperdiet eu eu mi. Sed aliquam non turpis non elementum. Praesent tempus, felis nec porttitor posuere, nisi ante pulvinar elit, vitae semper justo justo luctus urna. Proin dignissim nisi ut eros mollis ultricies. Sed tincidunt metus quis euismod gravida. Aenean ac dictum mi, sed consequat nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi feugiat, sem vel vehicula egestas, diam neque sollicitudin nulla, cursus tristique tellus magna id erat. Nam congue urna at posuere blandit. Duis a enim id arcu ullamcorper rhoncus. Aliquam sit amet nisl sed libero pellentesque volutpat id eu magna. In viverra malesuada neque quis commodo. Fusce quis rutrum turpis. Aliquam erat volutpat. Suspendisse gravida ipsum turpis, sed finibus quam tristique sit amet. Etiam accumsan dolor justo, ut tempor elit fringilla eget. Nunc iaculis felis est, vel tristique purus lobortis eget.",
  "images": [
    "https://via.placeholder.com/350x150",
    "https://via.placeholder.com/350x150",
    "https://via.placeholder.com/350x150"
  ]
}
*/
server.get('/event/:eventId/details', (req, res) => {
  const JSONData = getJSONData()
  const eventObj = JSONData["events"].find(obj => obj.id === getIdFromURL(req, 2))
  res.json({
    start_time: eventObj.start_time,
    end_time: eventObj.end_time,
    location: eventObj.location,
    description: eventObj.description,
    images: [
      "https://via.placeholder.com/350x150",
      "https://via.placeholder.com/350x150",
      "https://via.placeholder.com/350x150",
      "https://via.placeholder.com/350x150",
      "https://via.placeholder.com/350x150",
      "https://via.placeholder.com/350x150",
    ]
  })
})

/*
GET Specific Event Participants. Example Response:
{
  "participants": [
    {
      "id": 101,
      "username": "conan_edogawa",
      "avatar": "http://i.pravatar.cc/300?u=ConanEdogawa"
    }
  ],
  "participants_count": 1
}
*/
server.get('/event/:eventId/participants', (req, res) => {
  const JSONData = getJSONData();
  const eventObj = JSONData["events"].find(obj => obj.id === getIdFromURL(req, 2))
  let participants = JSONData["participants"].filter(obj => {
    return obj.event_id === eventObj.id
  })
  res.json({
    participants: getUserDetails(participants),
    participants_count: getUserDetails(participants).length,
  })
})

/*
GET Specific Event Likes. Example Response:
{
  "likes": [
    {
      "id": 101,
      "username": "conan_edogawa",
      "avatar": "http://i.pravatar.cc/300?u=ConanEdogawa"
    }
  ],
  "likes_count": 1
}
*/
server.get('/event/:eventId/likes', (req, res) => {
  const JSONData = getJSONData()
  const eventObj = JSONData["events"].find(obj => obj.id === getIdFromURL(req, 2))
  let likes = JSONData["likes"].filter(obj => {
    return obj.event_id === eventObj.id
  })  
  res.json({
    likes: getUserDetails(likes),
    likes_count: getUserDetails(likes).length
  })
})

/*
GET Specific Event Comments (Comments Subpage). Example Response:
{
  "comments": [
    {
      "id": 10001,
      "event_id": 1001,
      "user_id": 101,
      "comment": "Awesome, can't wait!",
      "create_at": "2022-10-15T10:00:00Z",
      "creator": {
        "id": 101,
        "username": "conan_edogawa",
        "avatar": "http://i.pravatar.cc/300?u=ConanEdogawa"
      }
    }
  ],
  "comments_count": 1
}
*/
server.get('/event/:eventId/comments', (req, res) => {
  const JSONData = getJSONData()
  const eventId = getIdFromURL(req, 2)  
  const comments = JSONData["comments"].filter(obj => {
    return obj.event_id === eventId
  })
  comments.forEach((comment) => {
    comment['creator'] = getMiniUserDetailsObj(comment)
  });
  res.json({
    comments: comments,
    comments_count: comments.length
  })
})

/*
GET Information about whether user already liked or already joined the specific event. Example response:
{
  "is_current_user_participant": 1,
  "is_current_user_like": 1
}
*/
server.get('/event/:eventId/activities/:userId', (req, res) => {
  const eventId = getIdFromURL(req, 2)
  userId =  getIdFromURL(req, 4)
  res.json(getIsUserParticipatedOrLikedAnEvent(eventId, userId))
})


/*
POST Specific Event Participants From Current User. Success will return HTTP Code 200
*/
server.post('/event/:eventId/participants', (req, res) => {
  doWriteToJSON(generateEventParticipationOrLikeObj(req, "participants"))
  res.sendStatus(200)
})

/*
POST Specific Event Likes From Current User. Success will return HTTP Code 200
*/
server.post('/event/:eventId/likes', (req, res) => {
  doWriteToJSON(generateEventParticipationOrLikeObj(req, "likes"))
  res.sendStatus(200)
})

/*
POST Specific Event Comments (Comments Subpage). Success will return HTTP Code 200
*/
server.post('/event/:eventId/comments', (req, res) => {
  const JSONData = getJSONData()
  JSONData["comments"].push(
    {
      id: JSONData["comments"][JSONData["comments"].length - 1].id + 1,
      event_id: getIdFromURL(req, 2),
      user_id: req.body.user_id,
      comment: req.body.comment,
      create_time: moment().utc().format("YYYY-MM-DDTHH:mm:ss[Z]")
    }
  )
  doWriteToJSON(JSONData)
  res.sendStatus(200)
})

/*
DELETE Specific Event Participants From Current User. Success will return HTTP Code 200
*/
server.delete('/event/:eventId/participants/:userId', (req, res) => {
  doWriteToJSON(generateNewParticipantsOrLikesByUserId(req, "participants"))
  res.sendStatus(200)
})

/*
DELETE Specific Event Likes From Current User. Success will return HTTP Code 200
*/
server.delete('/event/:eventId/likes/:userId', (req, res) => {
  doWriteToJSON(generateNewParticipantsOrLikesByUserId(req, "likes"))
  res.sendStatus(200)
})

// ========== USER SPECIFIC APIs ========== //

/*
GET User's Profile. Example Response:
{
  "id": 101,
  "fullname": "Conan Edogawa",
  "username": "conan_edogawa",
  "email": "conan.edogawa@email.com",
  "avatar": "http://i.pravatar.cc/300?u=ConanEdogawa"
}
*/
server.get('/user/:userId/', (req, res) => {
  const JSONData = getJSONData();
  const userObj = JSONData["users"].find(obj => obj.id === getIdFromURL(req, 2))
  delete userObj["password"]
  res.json(userObj)
})

/*
GET User's Participant, Likes, and Past Event Counter. Example Response:
{
  "likes_count": 2,
  "participants_count": 3,
  "pasts_count": 0
}
*/
server.get('/user/:userId/counters', (req, res) => {
  const JSONData = getJSONData();
  const userObj = JSONData["users"].find(obj => obj.id === getIdFromURL(req, 2))
  let likes = JSONData["likes"].filter(obj => {
    return obj.user_id === userObj.id
  })
  let participants = JSONData["participants"].filter(obj => {
    return obj.user_id === userObj.id
  })
  res.json({
    likes_count: likes.length,
    participants_count: participants.length,
    pasts_count: 0
  })
})

/*
GET User's Events as a Participant. Example Response:
[
  {
    "event_id": 1001,
    "user_id": 101,
    "event": {
      "channel_id": 1,
      "title": "Annual Gaming Exhibition 2022",
      "start_time": "2022-12-01T10:00:00Z",
      "end_time": "2022-12-11T10:00:00Z",
      "description": "EVENT 1001 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam pellentesque sollicitudin nunc id tristique. Morbi ut enim nec eros semper imperdiet eu eu mi. Sed aliquam non turpis non elementum. Praesent tempus, felis nec porttitor posuere, nisi ante pulvinar elit, vitae semper justo justo luctus urna. Proin dignissim nisi ut eros mollis ultricies. Sed tincidunt metus quis euismod gravida. Aenean ac dictum mi, sed consequat nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi feugiat, sem vel vehicula egestas, diam neque sollicitudin nulla, cursus tristique tellus magna id erat. Nam congue urna at posuere blandit. Duis a enim id arcu ullamcorper rhoncus. Aliquam sit amet nisl sed libero pellentesque volutpat id eu magna. In viverra malesuada neque quis commodo. Fusce quis rutrum turpis. Aliquam erat volutpat. Suspendisse gravida ipsum turpis, sed finibus quam tristique sit amet. Etiam accumsan dolor justo, ut tempor elit fringilla eget. Nunc iaculis felis est, vel tristique purus lobortis eget.",
      "channel_name": "Gaming",
      "creator": {
        "id": 101,
        "username": "conan_edogawa",
        "avatar": "http://i.pravatar.cc/300?u=ConanEdogawa"
      }
    }
  }
]
*/
server.get('/user/:userId/participants', (req, res) => {
  res.json(generateEventAndChannelForParticipantAndLikes(req, "participants"))
})

/*
GET User's Liked Events. Example Response:
[
  {
    "event_id": 1001,
    "user_id": 101,
    "event": {
      "channel_id": 1,
      "title": "Annual Gaming Exhibition 2022",
      "start_time": "2022-12-01T10:00:00Z",
      "end_time": "2022-12-11T10:00:00Z",
      "description": "EVENT 1001 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam pellentesque sollicitudin nunc id tristique. Morbi ut enim nec eros semper imperdiet eu eu mi. Sed aliquam non turpis non elementum. Praesent tempus, felis nec porttitor posuere, nisi ante pulvinar elit, vitae semper justo justo luctus urna. Proin dignissim nisi ut eros mollis ultricies. Sed tincidunt metus quis euismod gravida. Aenean ac dictum mi, sed consequat nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi feugiat, sem vel vehicula egestas, diam neque sollicitudin nulla, cursus tristique tellus magna id erat. Nam congue urna at posuere blandit. Duis a enim id arcu ullamcorper rhoncus. Aliquam sit amet nisl sed libero pellentesque volutpat id eu magna. In viverra malesuada neque quis commodo. Fusce quis rutrum turpis. Aliquam erat volutpat. Suspendisse gravida ipsum turpis, sed finibus quam tristique sit amet. Etiam accumsan dolor justo, ut tempor elit fringilla eget. Nunc iaculis felis est, vel tristique purus lobortis eget.",
      "channel_name": "Gaming",
      "creator": {
        "id": 101,
        "username": "conan_edogawa",
        "avatar": "http://i.pravatar.cc/300?u=ConanEdogawa"
      }
    }
  }
]
*/
server.get('/user/:userId/likes', (req, res) => {
  res.json(generateEventAndChannelForParticipantAndLikes(req, "likes"))
})

server.use(rewriter)
server.use(router)
server.listen(3001, () => {
  console.log('JSON Server is running now!')
  console.log('Current unix time is: ' + moment().unix())
})