# Black Cat (Backend)

Single Page App Based Social Event Sharing Platform

## Documentation

Please refer to this [documentation link](https://confluence.shopee.io/x/KdFiVg) for more information

Frontend Project please [click this link](https://gitlab.com/rd.pradipta/black-cat-frontend)

## Project Usage

Install the latest LTS version of Node.js and run this command

`npm install && node api/server.js`

This pplication will be ready to be accessed using port `3001`
